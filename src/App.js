import React from "react";
import Number from "./Number/Number";
import './App.css';

class App extends React.Component {
    state = {
        numbers: [5, 6, 7, 8, 9]
    };

    changeNumber = () => {
        const number = this.state.numbers.map(numbers => {
            while (true) {
                return Math.floor(Math.random() * (36 - 5 + 1) + 5)
                if (number >= number) {
                    continue;
                } else if (number <= number) {
                    break;
                }
            }
        });
        this.setState({numbers: number});
    };

    render() {
        return (
            <div className="App">
                <div>
                    <button onClick={this.changeNumber}>New Numbers</button>
                </div>

                <div className="number-content">
                    <Number number={this.state.numbers[0]}/>
                    <Number number={this.state.numbers[1]}/>
                    <Number number={this.state.numbers[2]}/>
                    <Number number={this.state.numbers[3]}/>
                    <Number number={this.state.numbers[4]}/>
                </div>

            </div>
        );
    };
};

export default App;
