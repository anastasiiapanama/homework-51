import React from 'react';

const Number = props => {
    return (
        <div className="number">
            <p>{props.number}</p>
        </div>
    );
};

export default Number;